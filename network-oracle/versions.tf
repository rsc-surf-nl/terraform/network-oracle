terraform {
  required_providers {
    oci = {
      source  = "oracle/oci"
      version = "~> 6.1.0"
    }
    tls = {
      source  = "hashicorp/tls"
      version = "~> 4.0.5"
    }
  }
  required_version = ">= 1.6"
}
