output "network_id" {
  value = oci_core_vcn.vcn-oracle.id
}

output "id" {
  value = oci_core_vcn.vcn-oracle.id
}

output "subnet_id" {
  value = oci_core_subnet.vm-subnet.id
}