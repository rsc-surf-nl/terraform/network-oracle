resource "oci_core_vcn" "vcn-oracle" {

  compartment_id = var.compartment_id
  cidr_blocks    = [var.cidr]
  freeform_tags = {
    subscription       = var.subscription,
    application_type   = var.application_type,
    cloud_type         = var.cloud_type,
    resource_type      = var.resource_type,
    subscription_group = var.subscription_group,
    workspace_id       = var.workspace_id,
    wallet_id          = var.wallet_id
  }
}


resource "oci_core_subnet" "vm-subnet" {

  cidr_block     = var.cidr
  compartment_id = var.compartment_id
  vcn_id         = oci_core_vcn.vcn-oracle.id
  route_table_id = oci_core_route_table.vm-igw-rt.id
  freeform_tags = {
    subscription       = var.subscription,
    application_type   = var.application_type,
    cloud_type         = var.cloud_type,
    resource_type      = var.resource_type,
    subscription_group = var.subscription_group,
    workspace_id       = var.workspace_id,
    wallet_id          = var.wallet_id
  }
}


resource "oci_core_internet_gateway" "vm-igw" {

  compartment_id = var.compartment_id
  vcn_id         = oci_core_vcn.vcn-oracle.id
  enabled        = true
}


resource "oci_core_route_table" "vm-igw-rt" {

  compartment_id = var.compartment_id
  vcn_id         = oci_core_vcn.vcn-oracle.id
  route_rules {
    network_entity_id = oci_core_internet_gateway.vm-igw.id
    destination       = "0.0.0.0/0"
  }

  freeform_tags = {
    subscription       = var.subscription,
    application_type   = var.application_type,
    cloud_type         = var.cloud_type,
    resource_type      = var.resource_type,
    subscription_group = var.subscription_group,
    workspace_id       = var.workspace_id,
    wallet_id          = var.wallet_id
  }
}